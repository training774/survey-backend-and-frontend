FROM openjdk:17-jdk-slim-buster
#COPY target/learn-0.0.1-SNAPSHOT.jar learn-0.0.1-SNAPSHOT.jar
#COPY target/First.java First.java
ENTRYPOINT ["java","-jar","learn-0.0.1-SNAPSHOT.jar"]
#ENTRYPOINT ["javac","First.java"]

#ENTRYPOINT ["java","First.java"]
EXPOSE 8081