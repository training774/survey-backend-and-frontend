import java.sql.*;

public class Runup {
   static final String DB_URL = "jdbc:mysql://localhost:3306/acabes";
   static final String USER = "root";
   static final String PASS = "1234";
   static final String QUERY = "SELECT id, first, last, age FROM acabes.doctors";

   public static void main(String[] args) {
      // Open a connection
      try(Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
         Statement stmt = conn.createStatement();
         ResultSet rs = stmt.executeQuery(QUERY);) {
         // Extract data from result set
         while (rs.next()) {
            // Retrieve by column name
            System.out.print("ID: " + rs.getInt("id"));
            System.out.print(", Age: " + rs.getInt("sal"));
            System.out.print(", First: " + rs.getString("fname"));
            System.out.println(", Last: " + rs.getString("lname"));
         }
      } catch (SQLException e) {
         e.printStackTrace();
      } 
   }
}
